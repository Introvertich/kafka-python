from django.conf import settings
from django.db import models
from django.utils import timezone


class Post(models.Model):
    date = models.DateTimeField()
    text = models.TextField()