from django.urls import path
from . import views

urlpatterns = [
    path('', views.post_list, name='post_list'),
    path('post/day/', views.post_by_day, name='post_by_day'),
    path('about', views.about, name='about'),
]