var positionXForCurrentScreen = getComputedStyle(document.body).backgroundPositionX;
var positionYForCurrentScreen = getComputedStyle(document.body).backgroundPositionY;

positionXForCurrentScreen = positionXForCurrentScreen.replace("%", "");
positionYForCurrentScreen = positionYForCurrentScreen.replace("%", "");

document.body.onmousemove = function (event) {

    var centerX = window.screen.width / 2;
    var centerY = window.screen.height / 2;
    
    document.body.style.backgroundPositionX = positionXForCurrentScreen - ((event.clientX - centerX) / 150) + "%";
    document.body.style.backgroundPositionY = positionYForCurrentScreen - ((event.clientY - centerY) / 300) + "%";
}

var calendar = document.getElementById("calendar");

document.getElementById("calendarRef").onclick = function () {
    calendar.style.visibility == "visible" ? calendar.style.visibility = "hidden" 
                                           : calendar.style.visibility = "visible";
}