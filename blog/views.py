from django.shortcuts import render
from django.utils import timezone
from .models import Post
from .forms import PostForm
from datetime import datetime

def post_list(request):
    today = datetime.today()
    posts = Post.objects.filter(date__day=today.day, date__month=today.month)
    return render(request, 'blog/post_list.html', {'posts': posts})

def about(request):
    return render(request, 'blog/about.html', {})

def post_by_day(request):
    form = PostForm(request.POST)
    date_value = form['date'].value()
    date = datetime.strptime(date_value, '%Y-%m-%d')
    posts = Post.objects.filter(date__day=date.day, date__month=date.month)
    return render(request, 'blog/post_list.html', {'posts': posts})